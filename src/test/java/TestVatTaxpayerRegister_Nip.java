import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

// https://wl-api.mf.gov.pl/ - strona z publicznym REST API

public class TestVatTaxpayerRegister_Nip {

    @Test(enabled = false)
    public void testVatTaxpayerRegister_valid_nip() {
        Response response = get("https://wl-api.mf.gov.pl/api/search/nip/7270126358?date=2021-03-15");
        String actualBody = response.getBody().asString();
        String actualData= response.path("result.requestDateTime");
        String actualId = response.path("result.requestId");
        String expectedBody = "{\"result\":{\"subject\":{\"name\":\"UNIQA TOWARZYSTWO UBEZPIECZEŃ SPÓŁKA AKCYJNA\",\"nip\":\"7270126358\",\"statusVat\":\"Czynny\",\"regon\":\"004275573\",\"pesel\":null,\"krs\":\"0000001201\",\"residenceAddress\":null,\"workingAddress\":\"GDAŃSKA 132, 90-520 ŁÓDŹ\",\"representatives\":[],\"authorizedClerks\":[],\"partners\":[],\"registrationLegalDate\":\"2003-03-13\",\"registrationDenialBasis\":null,\"registrationDenialDate\":null,\"restorationBasis\":null,\"restorationDate\":null,\"removalBasis\":null,\"removalDate\":null,\"accountNumbers\":[\"04103015080000000817583024\",\"04175000090000000032837627\",\"06175000090000000012953755\",\"07175000090000000007874138\",\"13103013350000000817583500\",\"13175010930000000039028697\",\"14109025900000000133756246\",\"15175000090000000009887229\",\"16124055851111000048841159\",\"18175000090000000009338446\",\"21175000090000000009887218\",\"22175000090000000007820615\",\"25175000090000000007820755\",\"26103015080000000817583016\",\"31175000090000000009452478\",\"31175000090000000009886068\",\"36175000090000000008411287\",\"38175010930000000007952357\",\"40175010930000000007859589\",\"42175000090000000006524044\",\"48103015080000000817583008\",\"49175000090000000005700582\",\"49175000090000000020863428\",\"55175000090000000021945258\",\"59124055851111001080044960\",\"60124010371111001057589148\",\"65175000090000000009887202\",\"68175000090000000011607055\",\"69109025900000000136763905\",\"74175010930000000001161903\",\"75175000090000000009886052\",\"77175000090000000009887277\",\"77175010930000000006697445\",\"78175000090000000007820771\",\"81175000090000000012953763\",\"84175010930000000007852298\",\"91175000090000000003809781\",\"95124010371111001057589294\"],\"hasVirtualAccounts\":true},\"requestId\":\""+actualId+"\",\"requestDateTime\":\""+actualData+"\"}}";

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void testVatTaxpayerRegister_empty_nip() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/nip/?date=2021-03-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-112");
//        Assert.assertEquals(actualRegon, "Pole 'NIP' nie może być puste.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testVatTaxpayerRegister_invalid_lenght_nip() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/nip/727012336358?date=2021-03-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-113");
//        Assert.assertEquals(actualRegon, "Pole 'NIP' ma nieprawidłową długość. Wymagane 10 znaków.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testVatTaxpayerRegister_invalid_characters_nip() {
        Response response = get("https://wl-api.mf.gov.pl/api/search/nip/727w126358?date=2021-03-15");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"code\":\"WL-114\",\"message\":\"Pole 'NIP' zawiera niedozwolone znaki. Wymagane tylko cyfry.\"}";

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 400);
    }

    @Test
    public void testVatTaxpayerRegister_invalid_number_nip() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/nip/7276126358?date=2021-03-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-115");
//        Assert.assertEquals(actualRegon, "Nieprawidłowy NIP.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testGetRequest_responseElements_invalid_date_in_the_future() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/nip/7270126358?date=2025-09-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-103");
//        Assert.assertEquals(actualRegon, "Data nie może być datą przyszłą.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testGetRequest_responseElements_invalid_date_in_the_past() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/nip/7270126358?date=1404-09-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-118");
        Assert.assertEquals(actualRegon, "Data sprzed zakresu rejestru.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }
}
