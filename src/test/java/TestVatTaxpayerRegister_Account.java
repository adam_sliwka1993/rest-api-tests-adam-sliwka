import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

// https://wl-api.mf.gov.pl/ - strona z publicznym REST API

public class TestVatTaxpayerRegister_Account {

    @Test(enabled = false)
    public void testVatTaxpayerRegister_valid_account() {
        Response response = get("https://wl-api.mf.gov.pl/api/search/bank-account/75175000090000000009886052?date=2021-03-15");
        String actualBody = response.getBody().asString();
        String actualData= response.path("result.requestDateTime");
        String actualId = response.path("result.requestId");
        String expectedBody = "{\"result\":{\"subjects\":[{\"name\":\"UNIQA TOWARZYSTWO UBEZPIECZEŃ SPÓŁKA AKCYJNA\",\"nip\":\"7270126358\",\"statusVat\":\"Czynny\",\"regon\":\"004275573\",\"pesel\":null,\"krs\":\"0000001201\",\"residenceAddress\":null,\"workingAddress\":\"GDAŃSKA 132, 90-520 ŁÓDŹ\",\"representatives\":[],\"authorizedClerks\":[],\"partners\":[],\"registrationLegalDate\":\"2003-03-13\",\"registrationDenialBasis\":null,\"registrationDenialDate\":null,\"restorationBasis\":null,\"restorationDate\":null,\"removalBasis\":null,\"removalDate\":null,\"accountNumbers\":[\"04103015080000000817583024\",\"04175000090000000032837627\",\"06175000090000000012953755\",\"07175000090000000007874138\",\"13103013350000000817583500\",\"13175010930000000039028697\",\"14109025900000000133756246\",\"15175000090000000009887229\",\"16124055851111000048841159\",\"18175000090000000009338446\",\"21175000090000000009887218\",\"22175000090000000007820615\",\"25175000090000000007820755\",\"26103015080000000817583016\",\"31175000090000000009452478\",\"31175000090000000009886068\",\"36175000090000000008411287\",\"38175010930000000007952357\",\"40175010930000000007859589\",\"42175000090000000006524044\",\"48103015080000000817583008\",\"49175000090000000005700582\",\"49175000090000000020863428\",\"55175000090000000021945258\",\"59124055851111001080044960\",\"60124010371111001057589148\",\"65175000090000000009887202\",\"68175000090000000011607055\",\"69109025900000000136763905\",\"74175010930000000001161903\",\"75175000090000000009886052\",\"77175000090000000009887277\",\"77175010930000000006697445\",\"78175000090000000007820771\",\"81175000090000000012953763\",\"84175010930000000007852298\",\"91175000090000000003809781\",\"95124010371111001057589294\"],\"hasVirtualAccounts\":true}],\"requestDateTime\":\""+actualData+"\",\"requestId\":\""+actualId+"\"}}";

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void testVatTaxpayerRegister_empty_account() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/bank-account/?date=2021-03-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-108");
//        Assert.assertEquals(actualRegon, "Pole 'numer konta' nie może być puste.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testVatTaxpayerRegister_invalid_lenght_account() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/bank-account/5175000090000000009886052?date=2021-03-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-109");
//        Assert.assertEquals(actualRegon, "Pole 'numer konta' ma nieprawidłową długość. Wymagane 26 znaków.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testVatTaxpayerRegister_invalid_characters_account() {
        Response response = get("https://wl-api.mf.gov.pl/api/search/bank-account/0617500!090000000012953755?date=2021-03-15");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"code\":\"WL-110\",\"message\":\"Pole 'numer konta' zawiera niedozwolone znaki. Wymagane tylko cyfry.\"}";

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 400);
    }

    @Test
    public void testVatTaxpayerRegister_invalid_number_account() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/bank-account/86175000090000000012953755?date=2021-03-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-111");
//        Assert.assertEquals(actualRegon, "Nieprawidłowy numer konta bankowego.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testGetRequest_responseElements_invalid_date_in_the_future() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/bank-account/75175000090000000009886052?date=2221-03-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-103");
//        Assert.assertEquals(actualRegon, "Data nie może być datą przyszłą.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testGetRequest_responseElements_invalid_date_in_the_past() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/search/bank-account/75175000090000000009886052?date=1221-03-15");
        String actualCode = actualResponse.path("code");
        String actualRegon = actualResponse.path("message");

        Assert.assertEquals(actualCode, "WL-118");
        Assert.assertEquals(actualRegon, "Data sprzed zakresu rejestru.");
        Assert.assertEquals(actualResponse.statusCode(), 400);
    }
}


