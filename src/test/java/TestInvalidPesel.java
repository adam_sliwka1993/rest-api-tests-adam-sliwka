import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class TestInvalidPesel {

    @Test
    public void testInvalidPesel_missing_number(){
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=");
        String actualType = actualResponse.path("type");
        String actualTitle = actualResponse.path("title");
        int actualStatus = actualResponse.path("status");

        Assert.assertEquals(actualType,"https://tools.ietf.org/html/rfc7231#section-6.5.1");
        Assert.assertEquals(actualTitle,"Bad Request");
        Assert.assertEquals(actualStatus,400);
        Assert.assertEquals(actualResponse.statusCode(), 400);
        Assert.assertNotEquals(actualResponse.statusCode(), 200);
    }

    @Test
    public void testInvalidPesel_checkSum_error() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=70081964744");
        String actualPesel = actualResponse.path("pesel");
        Boolean actualIsValid = actualResponse.path("isValid");
        String actualBirthDate = actualResponse.path("birthDate");
        String actualSex = actualResponse.path("sex");
        String actualErrorsCode = actualResponse.path("errors.errorCode").toString();
        String actualErrorsMessage = actualResponse.path("errors.errorMessage").toString();

        Assert.assertEquals(actualPesel,"70081964744");
        Assert.assertFalse(actualIsValid);
        Assert.assertEquals(actualBirthDate,"1970-08-19T00:00:00");
        Assert.assertEquals(actualSex,"Female");
        Assert.assertEquals(actualErrorsCode,"[INVC]");
        Assert.assertEquals(actualErrorsMessage,"[Check sum is invalid. Check last digit.]");
        Assert.assertEquals(actualResponse.statusCode(), 200);
        Assert.assertNotEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_characters_error() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=dfg543");
        String actualPesel = actualResponse.path("pesel");
        Boolean actualIsValid = actualResponse.path("isValid");
        String actualBirthDate = actualResponse.path("birthDate");
        String actualSex = actualResponse.path("sex");
        String actualErrorsCode = actualResponse.path("errors.errorCode").toString();
        String actualErrorsMessage = actualResponse.path("errors.errorMessage").toString();

        Assert.assertEquals(actualPesel,"dfg543");
        Assert.assertFalse(actualIsValid);
        Assert.assertEquals(actualBirthDate,null);
        Assert.assertEquals(actualSex,null);
        Assert.assertEquals(actualErrorsCode,"[NBRQ]");
        Assert.assertEquals(actualErrorsMessage,"[Invalid characters. Pesel should be a number.]");
        Assert.assertEquals(actualResponse.statusCode(), 200);
        Assert.assertNotEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_length_too_long_error() {
        String expectedBody = "{\"pesel\":\"890715204451\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":[{\"errorCode\":\"INVL\",\"errorMessage\":\"Invalid length. Pesel should have exactly 11 digits.\"}]}";
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=890715204451");
        String actualBody = response.getBody().asString();

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertNotEquals(response.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_length_too_short_error() {
        String expectedBody = "{\"pesel\":\"0715204451\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":[{\"errorCode\":\"INVL\",\"errorMessage\":\"Invalid length. Pesel should have exactly 11 digits.\"}]}";
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=0715204451");
        String actualBody = response.getBody().asString();

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertNotEquals(response.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_day_error() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=82022941170");
        String actualPesel = actualResponse.path("pesel");
        Boolean actualIsValid = actualResponse.path("isValid");
        String actualBirthDate = actualResponse.path("birthDate");
        String actualSex = actualResponse.path("sex");
        String actualErrorsCode = actualResponse.path("errors.errorCode").toString();
        String actualErrorsMessage = actualResponse.path("errors.errorMessage").toString();

        Assert.assertEquals(actualPesel,"82022941170");
        Assert.assertFalse(actualIsValid);
        Assert.assertEquals(actualBirthDate,null);
        Assert.assertEquals(actualSex,"Male");
        Assert.assertEquals(actualErrorsCode,"[INVD]");
        Assert.assertEquals(actualErrorsMessage,"[Invalid day.]");
        Assert.assertEquals(actualResponse.statusCode(), 200);
        Assert.assertNotEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_checkSum_year_month_day_error() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=03000012231");
        String actualPesel = actualResponse.path("pesel");
        Boolean actualIsValid = actualResponse.path("isValid");
        String actualBirthDate = actualResponse.path("birthDate");
        String actualSex = actualResponse.path("sex");
        String actualErrorsCode = actualResponse.path("errors.errorCode").toString();
        String actualErrorsMessage = actualResponse.path("errors.errorMessage").toString();

        Assert.assertEquals(actualPesel,"03000012231");
        Assert.assertFalse(actualIsValid);
        Assert.assertEquals(actualBirthDate,null);
        Assert.assertEquals(actualSex,"Male");
        Assert.assertEquals(actualErrorsCode,"[INVC, INVY, INVM, INVD]");
        Assert.assertEquals(actualErrorsMessage,"[Check sum is invalid. Check last digit., Invalid year., Invalid month., Invalid day.]");
        Assert.assertEquals(actualResponse.statusCode(), 200);
        Assert.assertNotEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_checkSum_day_error() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=93823012231");
        String actualPesel = actualResponse.path("pesel");
        Boolean actualIsValid = actualResponse.path("isValid");
        String actualBirthDate = actualResponse.path("birthDate");
        String actualSex = actualResponse.path("sex");
        String actualErrorsCode = actualResponse.path("errors.errorCode").toString();
        String actualErrorsMessage = actualResponse.path("errors.errorMessage").toString();

        Assert.assertEquals(actualPesel,"93823012231");
        Assert.assertFalse(actualIsValid);
        Assert.assertEquals(actualBirthDate,null);
        Assert.assertEquals(actualSex,"Male");
        Assert.assertEquals(actualErrorsCode,"[INVC, INVD]");
        Assert.assertEquals(actualErrorsMessage,"[Check sum is invalid. Check last digit., Invalid day.]");
        Assert.assertEquals(actualResponse.statusCode(), 200);
        Assert.assertNotEquals(actualResponse.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_year_month_day_error() {
        String expectedBody = "{\"pesel\":\"97004824231\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Male\",\"errors\":[{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"},{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=97004824231");
        String actualBody = response.getBody().asString();

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertNotEquals(response.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_year_month_error() {
        String expectedBody = "{\"pesel\":\"00142592986\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Female\",\"errors\":[{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}";
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00142592986");
        String actualBody = response.getBody().asString();

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertNotEquals(response.statusCode(), 400);
    }

    @Test
    public void testInvalidPesel_checkSum_year_month_error() {
        String expectedBody = "{\"pesel\":\"00142592987\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Female\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"},{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}";
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00142592987");
        String actualBody = response.getBody().asString();

        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertNotEquals(response.statusCode(), 400);
    }
}

