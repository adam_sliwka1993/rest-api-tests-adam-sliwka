import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class TestValidPesel {

    @Test
    public void testValidPesel_1800_01_01() {
        Response actualResponseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00810180044");
        String actualPeselYear = actualResponseYear.path("pesel");
        Boolean actualIsValidYear = actualResponseYear.path("isValid");
        String actualBirthDateYear = actualResponseYear.path("birthDate");
        String actualSexYear = actualResponseYear.path("sex");
        String actualErrorsYear = actualResponseYear.path("errors").toString();

        Assert.assertEquals(actualPeselYear,"00810180044");
        Assert.assertTrue(actualIsValidYear);
        Assert.assertEquals(actualBirthDateYear,"1800-01-01T00:00:00");
        Assert.assertEquals(actualSexYear,"Female");
        Assert.assertEquals(actualErrorsYear,"[]");
        Assert.assertEquals(actualResponseYear.statusCode(), 200);
        Assert.assertNotEquals(actualResponseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_1899_12_31() {
        Response actualResponseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99923180094");
        String actualPeselYear= actualResponseYear.path("pesel");
        Boolean actualIsValidYear = actualResponseYear.path("isValid");
        String actualBirthDateYear= actualResponseYear.path("birthDate");
        String actualSexYear = actualResponseYear.path("sex");
        String actualErrorsYear= actualResponseYear.path("errors").toString();

        Assert.assertEquals(actualPeselYear,"99923180094");
        Assert.assertTrue(actualIsValidYear);
        Assert.assertEquals(actualBirthDateYear,"1899-12-31T00:00:00");
        Assert.assertEquals(actualSexYear,"Male");
        Assert.assertEquals(actualErrorsYear,"[]");
        Assert.assertEquals(actualResponseYear.statusCode(), 200);
        Assert.assertNotEquals(actualResponseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_1900_01_01() {
        Response actualResponseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00010180095");
        String actualPeselYear= actualResponseYear.path("pesel");
        Boolean actualIsValidYear = actualResponseYear.path("isValid");
        String actualBirthDateYear= actualResponseYear.path("birthDate");
        String actualSexYear = actualResponseYear.path("sex");
        String actualErrorsYear= actualResponseYear.path("errors").toString();

        Assert.assertEquals(actualPeselYear,"00010180095");
        Assert.assertTrue(actualIsValidYear);
        Assert.assertEquals(actualBirthDateYear,"1900-01-01T00:00:00");
        Assert.assertEquals(actualSexYear,"Male");
        Assert.assertEquals(actualErrorsYear,"[]");
        Assert.assertEquals(actualResponseYear.statusCode(), 200);
        Assert.assertNotEquals(actualResponseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_1999_12_31() {
        Response actualResponseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99123180021");
        String actualPeselYear= actualResponseYear.path("pesel");
        Boolean actualIsValidYear = actualResponseYear.path("isValid");
        String actualBirthDateYear= actualResponseYear.path("birthDate");
        String actualSexYear = actualResponseYear.path("sex");
        String actualErrorsYear= actualResponseYear.path("errors").toString();

        Assert.assertEquals(actualPeselYear,"99123180021");
        Assert.assertTrue(actualIsValidYear);
        Assert.assertEquals(actualBirthDateYear,"1999-12-31T00:00:00");
        Assert.assertEquals(actualSexYear,"Female");
        Assert.assertEquals(actualErrorsYear,"[]");
        Assert.assertEquals(actualResponseYear.statusCode(), 200);
        Assert.assertNotEquals(actualResponseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_2000_01_01() {
        Response actualResponseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00210180022");
        String actualPeselYear= actualResponseYear.path("pesel");
        Boolean actualIsValidYear = actualResponseYear.path("isValid");
        String actualBirthDateYear= actualResponseYear.path("birthDate");
        String actualSexYear = actualResponseYear.path("sex");
        String actualErrorsYear= actualResponseYear.path("errors").toString();

        Assert.assertEquals(actualPeselYear,"00210180022");
        Assert.assertTrue(actualIsValidYear);
        Assert.assertEquals(actualBirthDateYear,"2000-01-01T00:00:00");
        Assert.assertEquals(actualSexYear,"Female");
        Assert.assertEquals(actualErrorsYear,"[]");
        Assert.assertEquals(actualResponseYear.statusCode(), 200);
        Assert.assertNotEquals(actualResponseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_2099_12_31() {
        Response actualResponseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99323180010");
        String actualPeselYear= actualResponseYear.path("pesel");
        Boolean actualIsValidYear = actualResponseYear.path("isValid");
        String actualBirthDateYear= actualResponseYear.path("birthDate");
        String actualSexYear = actualResponseYear.path("sex");
        String actualErrorsYear= actualResponseYear.path("errors").toString();

        Assert.assertEquals(actualPeselYear,"99323180010");
        Assert.assertTrue(actualIsValidYear);
        Assert.assertEquals(actualBirthDateYear,"2099-12-31T00:00:00");
        Assert.assertEquals(actualSexYear,"Male");
        Assert.assertEquals(actualErrorsYear,"[]");
        Assert.assertEquals(actualResponseYear.statusCode(), 200);
        Assert.assertNotEquals(actualResponseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_2100_01_01() {
        String expectedBodyYear = "{\"pesel\":\"00410180059\",\"isValid\":true,\"birthDate\":\"2100-01-01T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Response responseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00410180059");
        String actualBodyYear = responseYear.getBody().asString();

        Assert.assertEquals(actualBodyYear, expectedBodyYear);
        Assert.assertEquals(responseYear.statusCode(), 200);
        Assert.assertNotEquals(responseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_2199_12_31() {
        String expectedBodyYear = "{\"pesel\":\"99523180085\",\"isValid\":true,\"birthDate\":\"2199-12-31T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Response responseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99523180085");
        String actualBodyYear = responseYear.getBody().asString();

        Assert.assertEquals(actualBodyYear, expectedBodyYear);
        Assert.assertEquals(responseYear.statusCode(), 200);
        Assert.assertNotEquals(responseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_2200_01_01() {
        String expectedBodyYear = "{\"pesel\":\"00610180086\",\"isValid\":true,\"birthDate\":\"2200-01-01T00:00:00\",\"sex\":\"Female\",\"errors\":[]}";
        Response responseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=00610180086");
        String actualBodyYear = responseYear.getBody().asString();

        Assert.assertEquals(actualBodyYear, expectedBodyYear);
        Assert.assertEquals(responseYear.statusCode(), 200);
        Assert.assertNotEquals(responseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_2299_12_31() {
        String expectedBodyYear = "{\"pesel\":\"99723180074\",\"isValid\":true,\"birthDate\":\"2299-12-31T00:00:00\",\"sex\":\"Male\",\"errors\":[]}";
        Response responseYear = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=99723180074");
        String actualBodyYear = responseYear.getBody().asString();

        Assert.assertEquals(actualBodyYear, expectedBodyYear);
        Assert.assertEquals(responseYear.statusCode(), 200);
        Assert.assertNotEquals(responseYear.statusCode(), 400);
    }

    @Test
    public void testValidPesel_leap_year() {
        Response actualResponse = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=12222957301");
        String actualPesel = actualResponse.path("pesel");
        Boolean actualIsValid = actualResponse.path("isValid");
        String actualBirthDate = actualResponse.path("birthDate");
        String actualSex = actualResponse.path("sex");
        String actualErrors = actualResponse.path("errors").toString();

        Assert.assertEquals(actualPesel,"12222957301");
        Assert.assertTrue(actualIsValid);
        Assert.assertEquals(actualBirthDate,"2012-02-29T00:00:00");
        Assert.assertEquals(actualSex,"Female");
        Assert.assertEquals(actualErrors,"[]");
        Assert.assertEquals(actualResponse.statusCode(), 200);
        Assert.assertNotEquals(actualResponse.statusCode(), 400);
    }
}
