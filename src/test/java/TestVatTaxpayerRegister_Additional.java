import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

// https://wl-api.mf.gov.pl/ - strona z publicznym REST API

public class TestVatTaxpayerRegister_Additional {

    @Test(enabled = false)
    public void testVatTaxpayerRegister_nip_with_account_check() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/check/nip/7270126358/bank-account/22175000090000000007820615");
        String actualAccountAssigned = actualResponse.path("result.accountAssigned");

        Assert.assertEquals(actualAccountAssigned, "TAK");
        Assert.assertEquals(actualResponse.statusCode(), 200);
    }

    @Test(enabled = false)
    public void testVatTaxpayerRegister_regon_with_account_check() {
        Response actualResponse = get("https://wl-api.mf.gov.pl/api/check/regon/004275573/bank-account/42175000090000000006524044");
        String actualAccountAssigned = actualResponse.path("result.accountAssigned");

        Assert.assertEquals(actualAccountAssigned, "TAK");
        Assert.assertEquals(actualResponse.statusCode(), 200);
    }

    @Test
    public void testVatTaxpayerRegister_invalid_request() {
        Response response = get("https://wl-api.mf.gov.pl/api");
        String actualBody = response.getBody().asString();
//        String expectedBody = "{\"code\":\"WL-190\",\"message\":\"Niepoprawne żądanie.\"}";

//        Assert.assertEquals(actualBody, expectedBody);
        Assert.assertEquals(response.statusCode(), 404);
    }
}



